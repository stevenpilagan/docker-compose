<?php
class Fruits {
    public static function getFruits() {
        return ['Apple', 'Banana', 'Orange', 'Grape', 'Strawberry', 'Mango'];
    }
}

echo Fruits::getFruits();